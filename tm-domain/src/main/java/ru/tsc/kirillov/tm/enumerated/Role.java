package ru.tsc.kirillov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Role {

    USUAL("Обычный пользователь"),
    ADMIN("Администратор");

    @Getter
    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty())
            return USUAL;
        for (Role role: values()) {
            if (role.name().equals(value))
                return role;
        }

        return USUAL;
    }

}
