package ru.tsc.kirillov.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.model.IWBS;
import ru.tsc.kirillov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO implements IWBS {

    @Nullable
    @Column(nullable = false, name = "user_id")
    private String userId;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date created = new Date();

    @Nullable
    @Column(name = "date_begin")
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date dateEnd;

    public AbstractUserOwnedModelDTO(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        this.userId = userId;
        this.name = name;
    }

    public AbstractUserOwnedModelDTO(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    public AbstractUserOwnedModelDTO(
            @NotNull final String name,
            @NotNull final Status status,
            @Nullable final Date dateBegin
    ) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public AbstractUserOwnedModelDTO(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return name + " : " + getStatus().getDisplayName() + " : " + description;
    }

}
