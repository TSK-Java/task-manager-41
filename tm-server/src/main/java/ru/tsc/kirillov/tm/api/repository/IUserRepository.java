package ru.tsc.kirillov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Insert("INSERT INTO user (id, login, password_hash, email, first_name, last_name, middle_name, role, locked)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull UserDTO model);

    @Override
    @Insert("INSERT INTO user (id, login, password_hash, email, first_name, last_name, middle_name, role, locked)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void addAll(@NotNull Collection<UserDTO> models);

    @Update("UPDATE user SET login = #{login}, password_hash = #{passwordHash}, email = #{email}, first_name = #{firstName},"
            + "last_name = #{lastName}, middle_name = #{middleName}, role = #{role}, locked = #{locked} "
            + "WHERE id = #{id}")
    int update(@NotNull UserDTO model);

    @Override
    @Delete("DELETE FROM user")
    void clear();

    @Override
    @NotNull
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<UserDTO> findAll();

    @Override
    @Select("SELECT count(1) = 1 FROM user WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Override
    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM user "
            + "WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneById(@Param("id")@NotNull String id);

    @Override
    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM user "
            + "LIMIT #{index}, 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneByIndex(@Param("index") @NotNull Integer index);

    @Override
    @Delete("DELETE FROM user WHERE id = #{id}")
    int remove(@NotNull UserDTO model);

    @Override
    void removeAll(@Nullable Collection<UserDTO> collection);

    @Override
    @Delete("DELETE FROM user WHERE id = #{id}")
    int removeById(@Param("id") @NotNull String id);

    @Override
    @Nullable
    @Delete("DELETE FROM user WHERE LIMIT #{index}, 1")
    int removeByIndex(@Param("index") @NotNull Integer index);

    @Override
    @Select("SELECT count(1) FROM user")
    long count();

    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM user "
            + "WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, locked FROM user "
            + "WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findByEmail(@Param("email") @NotNull String email);

    @Nullable
    @Delete("DELETE FROM user WHERE login = #{login}")
    UserDTO removeByLogin(@Param("login") @NotNull String login);

}
