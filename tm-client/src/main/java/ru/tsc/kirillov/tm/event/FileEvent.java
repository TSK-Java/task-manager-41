package ru.tsc.kirillov.tm.event;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.enumerated.FileEventKind;

import java.io.File;
import java.util.EventObject;

public class FileEvent extends EventObject {

    @Getter
    @NotNull
    private final FileEventKind kind;

    public FileEvent(@NotNull final File source, @NotNull final FileEventKind kind) {
        super(source);
        this.kind = kind;
    }

    @NotNull
    public File getFile() {
        return (File) getSource();
    }

}
